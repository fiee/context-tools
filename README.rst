Welcome!
========

These are just a few tools that are a bit more than one-off scripts; maybe you can use them.

| fiëé visuëlle
| Henning Hraban Ramm
| https://www.fiee.net


License
-------
Python License, unless otherwise mentioned.


Files
-----

:latin1_to_utf8.py:   convert text encoding - just rename to use other encodings!
:mab2bib/latex.py:    LaTeX encoding for converter script (copy the above script to e.g. latex_to_utf8.py)
:contextproject.py:   create parts of ConTeXt projects, i.e. products and components
:imgcheck.py:         check type and size of an image file (German output, requires PIL/Pillow)
:addtogit.py:         check files used by a ConTeXt job and add them to git (requires GitPython)

For the last one, use ``\enabledirectives[system.dumpfiles=json]`` to let ConTeXt create a .jlg file
that contains the list of files in JSON format. (This is similar to ``latexmk -recorder somelatexfile``.)
This is supported only by a LMTX version newer than 2024-07-20. The script falls back to the .log file.


/docx2ctx
---------

convert from DOCX to ConTeXt source


/mab2bib
--------

convert bibliography files from MAB (Maschinelles Austauschformat für Bibliotheken) to BIB (BibTeX),
see http://www.ctan.org/tex-archive/biblio/bibtex/utils/mab2bib/
