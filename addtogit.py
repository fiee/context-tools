#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Find files used by a ConTeXt run and commit them to the current git repository.

Use `\enabledirectives[system.dumpfiles=json]` to let ConTeXt create a .jlg file
that contains the list of files in JSON format.
If there’s none, the script falls back to parsing the .log file; then it can’t
cope with spaces in file paths.

Usage: {} <jobname>

Jobname is the ConTeXt file run, with or without extension.
This script looks for a .jlg or .log file with the same name.

If the current directory is not a git repository, it looks in parent directories.
"""
import os
import sys
from pathlib import Path
import json
import git
from pprint import pprint
directive = '\enabledirectives[system.dumpfiles=json]'
exclude_paths = (
    # only paths that are relative to the repository path are regarded at all
    #'/tex/texmf', # ConTeXt distribution
    #os.path.expanduser('~/texmf/'), # hometexmf
    #os.path.expanduser('~/Library/'), # MacOS stuff, e.g. fonts
    #'/Library/', # MacOS stuff, e.g. fonts
    '/fonts/', # any fonts
    '-temp-', # generated files (buffers)
    'm_k_i_v', # generated files
)
logkey='foundname'
cwd = os.getcwd()

if len(sys.argv) < 2:
    print(__doc__.format(sys.argv[0]))
    sys.exit(1)

job = Path(sys.argv[1])
jlg = job.with_suffix('.jlg')
log = job.with_suffix('.log')
trylog = True
list_of_files = []

if jlg.is_file():
    trylog = False
    with open(jlg, mode='r', encoding='utf-8') as j:
        try:
            jl = json.load(j)['files']
        except Exception as e:
            print(e)
            print('"{}" is not a valid JSON file. Did you use "{}"?'.format(jlg, directive))
            trylog = True
        for d in jl:
            # d should be a dict
            try:
                list_of_files.append(d[logkey])
            except TypeError as e:
                print(e)
                print(f)
                sys.exit(99)

if log.is_file() and trylog:
    with open(log, mode='r', encoding='utf-8') as l:
        for line in l.readlines():
            if not line.startswith('system') or not logkey in line:
                continue
            parts = line.split()
            for p in parts:
                if p.startswith(logkey):
                    path = Path(p.replace(logkey+'=',''))
                    if not path.is_file():
                        print('"{}" not found.'.format(path))
                    list_of_files.append(path)
elif trylog:
    print('Couldn’t find "{}" or "{}".'.format(jlg, log))
    sys.exit(3)

if not list_of_files:
    print('Couldn’t find any used files in "{}" or "{}".'.format(jlg, log))
    sys.exit(4)

repo = None
repodir = Path(cwd)
while not repo:
    try:
        repo = git.Repo(repodir)
    except git.exc.InvalidGitRepositoryError as e:
        repodir = repodir.parent
assert not repo.bare

good_files = set()
for ff in list_of_files:
    use_this = True
    if not Path(ff).is_relative_to(repodir):
        #print('"{}" excluded.'.format(ff))
        continue
    for exp in exclude_paths:
        if exp in str(ff):
            # can’t skip outer loop
            use_this = False
            #print('"{}" excluded.'.format(ff))
        if not Path(ff).is_file():
            use_this = False
            print('"{}" not found.'.format(ff))
    if use_this:
        good_files.add(ff)

print('Trying to add {} files to the repository.'.format(len(good_files)))
pprint(repo.index.add(good_files))
print('Git will ignore files it already knows. Please check and commit.')
